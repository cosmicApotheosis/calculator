Ethereum Blockchain Calculator
---
Welcome to the world's slowest calculator! The calculations are performed via a smart contract on the Ropsten Ethereum blockchain.

Try It
---

Download and install metamask: https://metamask.io/

Get some Ropsten ether: http://faucet.ropsten.be:3001/

Give it a try at: https://cosmicapotheosis.github.io/calculator/

Created with *create-react-app* and *truffle*.
