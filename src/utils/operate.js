export default function operate(numberOne, numberTwo, operation, contractInstance, web3) {
  // save ContractInstance state variable to new variable
  const calculatorV3 = contractInstance;

  web3.eth.getAccounts((error, accounts) => {
    // methods applied directly to state Contract instance
    // solidity doesnt support floating point or decimals so the inputs need to be multiplied and then divided in js file
    // Addition
    if (operation === '+') {
      return calculatorV3.addToNumber((numberOne * 1e10), (numberTwo * 1e10), { from: accounts[0], gasPrice: 50000000000 });
    }
    // Subtraction
    if (operation === '-') {
      return calculatorV3.substractNumber((numberOne * 1e10), (numberTwo * 1e10), { from: accounts[0], gasPrice: 50000000000 });
    }
    // Multiplication
    if (operation === 'x') {
      return calculatorV3.multiplyWithNumber((numberOne * 1e5), (numberTwo * 1e5), { from: accounts[0], gasPrice: 50000000000 });
    }
    // Division
    if (operation === '÷') {
      return calculatorV3.divideByNumber((numberOne * 1e15), (numberTwo * 1e5), { from: accounts[0], gasPrice: 50000000000 });
    }
    // Modulo
    if (operation === '%') {
      return calculatorV3.modulo((numberOne * 1e10), (numberTwo * 1e10), { from: accounts[0], gasPrice: 50000000000 });
    }
    throw Error(`Unknown operation '${operation}'`);
  })
}
