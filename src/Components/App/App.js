import './App.css';
import React from 'react';
import Display from '../Display/Display';
import ButtonPanel from '../ButtonPanel/ButtonPanel';
import calculate from '../../utils/calculate';
// this file needs to manually be copied over from the build directory into the src directory
// use truffle react box for next project to fix this and source straight from build folder
import CalculatorV3Contract from '../../CalculatorV3.json';
import getWeb3 from '../../utils/getWeb3';


class App extends React.Component {
  // Component constructor defines state and binds methods
  constructor(props) {
    super(props);
    this.state = {
      // Contract instance will be set in function below
      // to get this to work though you will have to recompile and migrate each time Ganache starts
      ContractInstance: null,
      total: null,
      next: null,
      operation: null,
      web3: null,
    };
    // bind methods here
  }

  componentWillMount() {
  // Get network provider and web3 instance.
  // See utils/getWeb3 for more info.

  getWeb3
    .then(results => {
      this.setState({
        web3: results.web3
      });

      // Instantiate contract once web3 provided.
      this.instantiateContract();
    })
    .catch(() => {
      console.log('Error finding web3.');
    });
  }

  instantiateContract() {

    const contract = require('truffle-contract');
    const calculatorV3 = contract(CalculatorV3Contract);
    calculatorV3.setProvider(this.state.web3.currentProvider);

    // Declaring this for later so we can chain functions on.
    let calculatorV3Instance;

    // Get accounts.
    this.state.web3.eth.getAccounts((error, accounts) => {
      calculatorV3.deployed().then((instance) => {
        calculatorV3Instance = instance;
        return this.setState({ ContractInstance: calculatorV3Instance });
      }).then(() => {
        return this.doContractEventWatchStart();
      }).then(() => {
        return calculatorV3Instance.result();
      }).then((result) => {
        // Update state with the result.
        // (result.args.n.toNumber() / 1e10).toString()
        //return this.setState({ total: (result.c[0].toNumber() / 1e10).toString() });
        return this.setState({ total: '0' });
        // set initial total to 0
      })
    })
  }

  // handleClick method passed as prop to child components
  handleClick = (buttonName) => {
    this.setState(calculate(this.state, buttonName));
  }

  // watch for smart contract events
  doContractEventWatchStart() {
    // store state contract instance in a seperate new variable
    let contractInstance = this.state.ContractInstance;

    // save all logs irrespective of input values
    let indexedEventValues = {

    }
    // start from this block since the first event happens shortly thereafter
    // for Ganache leave blank
    let additionalFilterOptions = {
      //"fromBlock":"2950000"
    }
    // contract event definition
    let contractEvent = contractInstance.NewTotal(indexedEventValues, additionalFilterOptions);

    // web3 contract watch callback function
    contractEvent.watch((error, result) => {
      if(error) {
        console.error('Contract Event Error');
      } else {
        // set the total state variable to newly captured log
        console.log("event log captured: ", result.event, "value: ", (result.args.n.toNumber() / 1e10).toString());
        this.setState({ total: (result.args.n.toNumber() / 1e10).toString() });
      }
    });
  }

  render() {
    return (
      <div className="component-app">
        <Display value={this.state.next || this.state.total || '0'} />
        <ButtonPanel clickHandler={this.handleClick} />
        {/*{ this.doContractEventWatchStart() }*/}
      </div>
    );
  }
}

export default App;
