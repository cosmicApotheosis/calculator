import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/App/App';
import './index.css';
//import 'github-fork-ribbon-css/gh-fork-ribbon.css';
import { Web3Provider } from 'react-web3';

ReactDOM.render(
  <Web3Provider>
    <App />
  </Web3Provider>,
  document.getElementById('root')
);
