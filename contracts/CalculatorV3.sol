pragma solidity ^0.4.6;

contract CalculatorV3 {

  int public result;
  /*
  event NumberAdded(uint n);
  event NumberSubtracted(uint n);
  event NumberMultiplied(uint n);
  event NumberDivided(uint n);
  */
  event NewTotal(int n);

  function CalculatorV3(int num) public {
    // constructor
    result=num;
  }

  // returns the result
  function getResult() public constant returns (int){
    return result;
  }

  // result = result + num
  function addToNumber(int num1, int num2) public returns (int) {
    result = num1 + num2;
    //NumberAdded(num);
    emit NewTotal(result);
    return result;
  }

  // result = result - num
  function substractNumber(int num1, int num2) public returns (int) {
    result = num1 - num2;
    //NumberSubtracted(num);
    emit NewTotal(result);
    return result;
  }

  // result = result * num
  function multiplyWithNumber(int num1, int num2) public returns (int) {
    result = num1 * num2;
    //NumberMultiplied(num);
    emit NewTotal(result);
    return result;
  }

  // result = result / num
  function divideByNumber(int num1, int num2) public returns (int) {
    result = num1 / num2;
    //NumberDivided(num);
    emit NewTotal(result);
    return result;
  }

  // result = result * 2
  function double() public returns (int) {
    result *= 2.;
    return result;
  }

  // result = result * 1/2
  function half() public returns (int) {
    result /= 2.;
    return result;
  }
  // result = result % num;
  function modulo(int num1, int num2) public returns (int) {
    result = num1 % num2;
    emit NewTotal(result);
    return result;
  }

}
