let CalculatorV3 = artifacts.require("../contracts/CalculatorV3");

function dumpEvents(result) {
  for (let i=0; i < result.logs.length; i++) {
        console.log(result.logs[i].event,'>>', result.logs[i].args.n.toNumber());
  }
}

contract('CalculatorV3', accounts => {

  it("should assert true", done => {
    let calculator_v3 = CalculatorV3.deployed();
    assert.isTrue(true);
    done();
  });
  // test addition function
  it("addition function works", () => {
    let calculator_v3;
    return CalculatorV3.deployed().then(instance => {
      calculator_v3 = instance;
      // add two numbers
      return calculator_v3.addToNumber(2, 2);
    }).then(() => {
      return calculator_v3.result();
    }).then(result => {
      assert.equal(result, 4);
    })
  });

  // test subtract function
  it("subtraction function works", () => {
    let calculator_v3;
    return CalculatorV3.deployed().then(instance => {
      calculator_v3 = instance;
      // subtract two numbers
      return calculator_v3.substractNumber(8, 5);
    }).then(() => {
      return calculator_v3.result();
    }).then(result => {
      assert.equal(result, 3);
    })
  });

  // test multiplication function
  it("multiplication function works", () => {
    let calculator_v3;
    return CalculatorV3.deployed().then(instance => {
      calculator_v3 = instance;
      // multiply two numbers
      return calculator_v3.multiplyWithNumber(10, 4);
    }).then(() => {
      return calculator_v3.result();
    }).then(result => {
      assert.equal(result, 40);
    })
  });

  // test division function
  it("division function works", () => {
    let calculator_v3;
    return CalculatorV3.deployed().then(instance => {
      calculator_v3 = instance;
      // divide two numbers
      return calculator_v3.divideByNumber(200, 40);
    }).then(() => {
      return calculator_v3.result();
    }).then(result => {
      assert.equal(result, 5);
    })
  });

  // test modulo function
  it("modulo function works", () => {
    let calculator_v3;
    return CalculatorV3.deployed().then(instance => {
      calculator_v3 = instance;
      // modulo two numbers
      return calculator_v3.modulo(13, 10);
    }).then(() => {
      return calculator_v3.result();
    }).then(result => {
      assert.equal(result, 3);
    })
  });
  // test for emitted events here
  it("addition function emits NewTotal event log", () => {
    let calculator_v3;
    return CalculatorV3.deployed().then(instance => {
      calculator_v3 = instance;
      // add two numbers
      return calculator_v3.addToNumber(2, 2);
    }).then(result => {
      // result=tx reciept that has the log/events
      dumpEvents(result);
      assert.equal('NewTotal',result.logs[0].event );
      assert.equal(4, result.logs[0].args.n.toNumber());
    })
  });

});
